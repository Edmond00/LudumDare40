package gmtk;

import processing.core.*;

public class Bamboo extends Takeable {

	Bamboo(Lac lac, int x, int y) {
		super("bamboo", lac, x, y);
		for (int i = 0; i < Game.app.rand(2,6); i++)
			new Pixel(Pixel.Type.BAMBOO, this, 0, -i);
	}
}
