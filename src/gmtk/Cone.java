package gmtk;

import processing.core.*;

public class Cone extends Takeable {

	Cone(Foliage foliage, int x, int y) {
		super("pine cone", foliage, x, y);
		new Pixel(Pixel.Type.CONE, this, 0, 0);
	}
}

