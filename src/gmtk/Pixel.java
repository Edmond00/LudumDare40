package gmtk;

import processing.core.*;

public class Pixel implements Comparable<Pixel> {

		public enum Block {
			FULL(),
			SEMI(),
			EMPTY();
		}

		public enum Type {
			HEAD(true, false, true, 0, Color.SKIN),
			HAND(0, Color.SKIN),
			BODY(false, false, true, 1, Color.BLACK),
			HIP(false, true, true, 1, Color.INVISIBLE),
			HAT(1, Color.BLACK),
			SHOE(false, false, false, 1, Color.BLACK),
			WOOD(11, Color.WOOD, Block.SEMI),
			CHERRYWOOD(11, Color.CHERRYWOOD, Block.SEMI),
			CONEWOOD(11, Color.CONEWOOD, Block.SEMI),
			OLIVEWOOD(11, Color.OLIVEWOOD, Block.SEMI),
			LEAF(-10, Color.LEAF),
			DEADLEAF(-11, Color.LEAF),
			CHERRYLEAF(-10, Color.CHERRYLEAF),
			CHERRYDEADLEAF(-11, Color.CHERRYLEAF),
			CONELEAF(-10, Color.CONELEAF),
			CONEDEADLEAF(-11, Color.CONELEAF),
			OLIVELEAF(-10, Color.OLIVELEAF),
			OLIVEDEADLEAF(-11, Color.OLIVELEAF),
			WATER(-10, Color.WATER),
			SPLASH(-10, Color.WATER),
			FLUSH(-10, Color.SAND),
			FLUSHSNOW(-10, Color.SNOW),
			HERB(9, Color.HERB, Block.FULL),
			SNOW(9, Color.SNOW, Block.FULL),
			STONE(9, Color.STONE, Block.FULL),
			APPLE(-11, Color.APPLE),
			CHERRY(-11, Color.CHERRY),
			CONE(-11, Color.CONE),
			OLIVE(-11, Color.OLIVE),
			STALK(-9, Color.HERB),
			BAMBOO(-9, Color.BAMBOO),
			WHEAT(-9, Color.WHEAT),
			SUGAR(-9, Color.SUGAR),
			FLOWER(-9, Color.HERB),
			SAND(9, Color.SAND, Block.FULL),
			DEEPEARTH(9, Color.DEEPEARTH, Block.FULL),
			EARTH(10, Color.EARTH, Block.FULL);
			
			public int priority;
			public Block block;
			public Color color;
			boolean collisionUp, collisionDown, collisionSide;
			Type(int priority, Color c) {
				this.priority = priority;
				this.color = c;
				this.block = Block.EMPTY;
				this.collisionDown = false;
				this.collisionUp = false;
				this.collisionSide = false;
			}
			Type(boolean cu, boolean cd, boolean cs, int priority, Color c) {
				this.priority = priority;
				this.color = c;
				this.block = Block.EMPTY;
				this.collisionDown = cd;
				this.collisionUp = cu;
				this.collisionSide = cs;
			}
			Type(int priority, Color c, Block block) {
				this.priority = priority;
				this.color = c;
				this.block = block;
			}
		}

		public Object from;
		public int rr, rg, rb, ra;
		public Type type;
		int x, y;
		boolean toRemove;

		public Pixel(Type t, Object o, int x, int y) {
			this.toRemove = false;
			this.type = t;
			this.from = o;
			this.x = x;
			this.y = y;
			this.rr = (int)Game.app.random(9000);
			this.rg = (int)Game.app.random(9000);
			this.rb = (int)Game.app.random(9000);
			this.ra = (int)Game.app.random(9000);
			o.pixels.add(this);
		}

		public Pixel(Type t, Pixel old) {
			this(t, old, true);
		}
		public Pixel(Pixel old, boolean removeOld) {
			this(old.type, old, removeOld);
		}
		public Pixel(Type t, Pixel old, boolean removeOld) {
			this.toRemove = false;
			this.type = t;
			this.from = old.from;
			this.x = old.x;
			this.y = old.y;
			this.rr = old.rr;
			this.rg = old.rg;
			this.rb = old.rb;
			this.ra = old.ra;
			if (removeOld)
				this.from.pixels.remove(old);
			this.from.pixels.add(this);
		}
		
		public int compareTo(Pixel p) {
			return p.type.priority - type.priority;
		}

		public int getX() {
			return from.getX(x);
		}

		public int getY() {
			return from.getY(y);
		}

		public void remove() {
			toRemove = true;
//			from.pixels.remove(this);
		}

		public void special() {
		}

		public void update() {
			special();
			if (type.collisionDown)
				from.touchGround(from.screen.isBlock(getX(), getY()+1) != Block.EMPTY);
			if (type.collisionUp)
				from.touchCeilling(from.screen.isBlock(getX(), getY()-1) == Block.FULL);
			if (type.collisionSide)
				from.touchLeftWall(from.screen.isBlock(getX()-1, getY()) == Block.FULL);
			if (type.collisionSide)
				from.touchRightWall(from.screen.isBlock(getX()+1, getY()) == Block.FULL);
		}

		public boolean draw(Rect r) {
			Rect r2;
			switch(type) {
				case WHEAT:
				case SUGAR:
				case BAMBOO:
					Game.app.drawRect(r, type.color, this);
					r2 = new Rect(r);
					r2.x += r.w / 3;
					r2.w /= 3;
					Game.app.drawRectAlpha(r2, type.color, this, 255);
					break;
				case STALK:
					r2 = new Rect(r);
					r2.x += r.w / 3;
					r2.w /= 3;
					Game.app.drawRect(r2, type.color, this);
					break;
				case FLOWER:
					Game.app.drawRect(r, from.getColor(), this);
					if (from.screen.pw + from.screen.ph > 10) {
						r2 = new Rect(r);
						r2.x += r.w / 3;
						r2.y += r.h / 3;
						r2.w -= r.w / 3*2;
						r2.h -= r.h / 3*2;
						Game.app.drawRect(r2, Color.YELLOW, this);
					}
					break;
				case HAT:
					Game.app.drawRect(r, type.color, this);
					r2 = new Rect(r);
					r2.x -= r.w / 2;
					r2.y += r.h / 2;
					r2.w *= 2;
					r2.h /= 2;
					Game.app.drawRect(r2, type.color, this);
					break;
				case HAND:
				case CHERRY:
				case SPLASH:
					r2 = new Rect(r);
					r2.x += r.w / 4;
					r2.y += r.h / 4;
					r2.w /= 2;
					r2.h /= 2;
					Game.app.drawRect(r2, type.color, this);
					break;
				case SHOE:
					r2 = new Rect(r);
					r2.y += r.h / 2;
					r2.h /= 2;
					Game.app.drawRect(r2, type.color, this);
					break;
				case WATER:
					Game.app.drawRect(r, type.color);
					break;
				default:
					Game.app.drawRect(r, type.color, this);
					break;
			}
			return true;
		}
}

