package gmtk;

import processing.core.*;
import java.util.ArrayList;
import java.util.List;

class Lac extends Ground {

		public Lac(MainScreen s, int n) {
			super(s, n);
			int begin = Game.app.rand(5, width/4);
			int end = Game.app.rand((width/4)*3, width-5);
			int lastBegin = begin;
			int lastEnd = end;
			int r;
			for (int j = lastHeigth; j < heigth; j++) {
				r = Game.app.rand(8);
				for (int i = 0; i < width; i++) {
					if (begin < end && r == 0 && i > lastBegin && i <= begin)
						new Bamboo(this, i, j - 1);
					if (begin < end && r == 1 && i < lastEnd && i >= end)
						new Bamboo(this, i, j - 1);
					if (i > begin && i < end)
						new Pixel(Pixel.Type.WATER, this, i, j);
					else if (i > begin - 3 && i < end + 3)
						new Pixel(Pixel.Type.SAND, this, i, j);
					else
						new Pixel(getEarthType(j - lastHeigth), this, i, j);
				}
				lastBegin = begin;
				lastEnd = end;
				begin += Game.app.rand(4);
				end -= Game.app.rand(4);

			}
		}

}

