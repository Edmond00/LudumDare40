package gmtk;

import processing.core.*;
import java.util.ArrayList;
import java.util.List;

abstract class Screen {

		String name;
		Game app;


		public Screen(Game a, String n, boolean firstScreen) {
			app = a;
			name = n;
			app.ls.add(this);
			if (firstScreen)
				app.actualScreen = this;
		}


		abstract void draw();
		abstract void codedEvent(int key);
		abstract void codedEventReleased(int key);
		abstract void asciiEvent(char key);
		abstract void asciiEventReleased(char key);


}
