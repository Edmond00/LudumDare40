package gmtk;

import processing.core.*;
import java.util.ArrayList;
import java.util.List;

class Hill extends Ground {
		
		void addRandomFlower(int x, int y) {
			int r = Game.app.rand(10);
			if (r < 7)
				new Flower(this, x, y, Flower.Type.WHITE);
			else if (r < 9)
				new Flower(this, x, y, Flower.Type.RED);
			else
				new Flower(this, x, y, Flower.Type.PINK);
		}

		public Hill(MainScreen s, int n) {
			super(s, n);
			int flat = 0;
			int lastFlower = 0;
			Tree.Type type;
			if (Game.app.rand(6) == 0)
				type = Tree.Type.CHERRY;
			else 
				type = Tree.Type.APPLE;
			for (int i = 0; i < width; i++) {
				if ((int)Game.app.random(5) % 5 == 0) {
					flat = 0;
					lastHeigth += ((int)Game.app.random(-5, 5) / 2);
					if (lastHeigth < 40)
						lastHeigth = 40;
					if (lastHeigth > 95)
						lastHeigth = 95;
				} else {
					flat++;
				}
				if (flat > 6 && (int)Game.app.random(3) == 0) {
					flat = 0;
					new Tree(this, i-6, lastHeigth - 1, type);
				} else if (lastFlower > 3 && Game.app.rand(6) == 0) {
					lastFlower = 0;
					addRandomFlower(i, lastHeigth - 1);
				}
				lastFlower +=1;
				for (int j = lastHeigth; j < heigth; j++) {
					new Pixel(getEarthType(j - lastHeigth), this, i, j);
				}
			}
		}

}

