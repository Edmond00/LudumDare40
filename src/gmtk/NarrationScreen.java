package gmtk;

import processing.core.*;
import java.util.LinkedList;
import java.util.Queue;

public class NarrationScreen extends Screen {
		
		Queue<Text> texts;
		int centerX;
		int commentY, textY;
		int commentSize, textSize;
		Text comment;

		public NarrationScreen(Game a, String n) {
			super(a, n, true);
			texts = new LinkedList<Text>();
			centerX = a.sw / 2;
			textY = a.sh / 2;
			commentY = a.sh / 6 * 4;
			textSize = 30;
			commentSize = 15;
			comment = new Text(centerX, commentY, "press ENTER to continue", commentSize, Color.COMMENT);
			pushText("He was a simple man...");
			pushText("...in a simple world.");
			pushText("A man who likes flower.");
		}

		public void pushText(String str) {
			texts.add(new Text(centerX, textY, str, textSize, Color.TEXT));
		}

		public void asciiEvent(char key) {
			if (key == 27) {
				app.exit();
			}
			if (key == '\n') {
				if (texts.poll() == null)
					app.changeScreen("main");
			}
		}
		public void asciiEventReleased(char key) {}

		public void codedEvent(int key) {
		}

		public void codedEventReleased(int key) {
		}

		public void drawBackground() {
			app.background(Color.BLACK.R,Color.BLACK.G,Color.BLACK.B);
		}

		void draw() {
			if (texts.peek() != null)
				texts.peek().draw();
			else
				app.changeScreen("main");
			comment.draw();
		}
}

