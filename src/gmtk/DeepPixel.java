package gmtk;

import processing.core.*;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Iterator;

class DeepPixel {

		PriorityQueue<Pixel> pixels;

		public DeepPixel(){
			pixels = new PriorityQueue<Pixel>();
		}

		void add(Pixel p) {
			pixels.add(p);
		}

		void draw(Rect r) {
			boolean continu = true;
			while (continu) {
				if (pixels.peek() == null)
					return ;
				continu = pixels.poll().draw(r);
			}
		}

		void clean() {
			pixels.clear();
		}

		Pixel.Block isBlock() {
			Pixel.Block r = Pixel.Block.EMPTY;
			Iterator<Pixel> it = pixels.iterator();
			while (it.hasNext()) {
				Pixel p = it.next();
				if (p.type.block == Pixel.Block.FULL)
					return p.type.block;
				if (p.type.block == Pixel.Block.SEMI)
					r = p.type.block;
			}
			return r;
		}

		public boolean isIn(Pixel.Type type) {
			Iterator<Pixel> it = pixels.iterator();
			while (it.hasNext()) {
				Pixel p = it.next();
				if (p.type == type)
					return true;
			}
			return false;
		}
}
