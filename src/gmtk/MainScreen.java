package gmtk;

import processing.core.*;
import java.util.ArrayList;
import java.util.List;

public class MainScreen extends Screen {
		
		Avatar avatar;
		List<Object> lo;
		DeepPixel[][] pixels;
		int w, h;
		Rect rect;
		int cameraX, cameraY;
		int pw;
		int ph;
		String actionMessage, hintMessage;
		static int numberGround = 150;

		void resize() {
			int tmpX = pw*w > app.sw ? 0 : app.sw /2 - pw*w/2;
			int tmpW = pw*w > app.sw ? app.sw : pw*w;
			int tmpY = ph*h > app.sh/2 ? app.sh/4 : app.sh/2 - ph*h/2;
			int tmpH = ph*h > app.sh/2 ? app.sh/2 : ph*h;
			tmpH = (tmpH / ph) * ph;
			this.rect = new Rect(tmpX, tmpY, tmpW, tmpH);
		}

		void generate() {
			avatar = new Avatar(this, w/2, 10);
//			avatar = new Avatar(this, 120*Ground.width, 10);
			for (int i = 0; i < numberGround; i++) {
				int r = Game.app.rand(100);
				if (i > numberGround - Ground.montainWidth) {
						new Mountain(this, i);
				} else if (i < Ground.desertWidth) {
					if (r < 20)
						new DesertLac(this, i);
					else
						new Desert(this, i);
				}
				else if (r < 15)
					new Lac(this, i);
				else if (r < 25) {
					int j;
					for (j = 0; i+j < numberGround && j < Game.app.rand(1, 4); j++)
						new Field(this, i+j);
					i += j-1;;
				}
				else
					new Hill(this, i);
			}
		}

		void printHeadMessage() {
			int x = app.sw / 2;
			int y = app.sh / 6;
			int size = 15;
			if (actionMessage != null) {
				app.printText(actionMessage, size, x, y, Color.TEXT);
			} else {
				app.printText(hintMessage, size, x, y, Color.COMMENT);
			}
		}

		public MainScreen(Game a, String n, int pw, int ph) {
			super(a, n, false);
			this.w = Ground.width * numberGround;
			this.h = Ground.heigth;
			this.pw = pw;
			this.ph = ph;
			this.cameraX = 0;
			this.cameraY = 0;
			this.hintMessage = new String("Take a white flower");
			this.actionMessage = null;
			resize();
			pixels = new DeepPixel[w][h];
			for (int i = 0; i < w; i++) {
				for (int j = 0; j < h; j++) {
					pixels[i][j] = new DeepPixel();
				}
			}
			lo = new ArrayList<Object>();
			generate();
		}

		public void asciiEvent(char key) {
			if (key == 27) {
				app.exit();
			} else if (key == ' ') {
				if (avatar.canBeTake != null) {
					Game.app.sounds.play("take");
					avatar.addItem(avatar.canBeTake.name);
					avatar.canBeTake.remove();
					avatar.canBeTake = null;
				}
			}
		}
		public void asciiEventReleased(char key) {}

		public void codedEvent(int key) {
			if (key == app.LEFT)
				avatar.move = Avatar.Move.LEFT;
			else if (key == app.RIGHT)
				avatar.move = Avatar.Move.RIGHT;
			if (key == app.UP && avatar.jumping == 0) {
				Game.app.sounds.play("jump");
				avatar.jumping = 1;
			}
		}

		public void codedEventReleased(int key) {
			if (key == app.LEFT && avatar.move == Avatar.Move.LEFT) {
				avatar.move = Avatar.Move.NO;
			}
			if (key == app.RIGHT && avatar.move == Avatar.Move.RIGHT)
				avatar.move = Avatar.Move.NO;
		}

		public void clean() {
			actionMessage = null;
			for (int i = 0; i < w; i++) {
				for (int j = 0; j < h; j++) {
					pixels[i][j].clean();
				}
			}
			for (Object o : lo) {
				o.cleanComposant();
			}
		}

		public int getW() {
			return (rect.w / pw) + 1;
		}

		public int getH() {
			return (rect.h / ph);
		}

		public void drawWorld() {
			drawBackground();
			Rect r = new Rect();
			r.w = pw;
			r.h = ph;
			for (int i = cameraX; i < cameraX + getW(); i++) {
				for (int j = cameraY; j < cameraY + getH(); j++) {
					if (i >= 0 && j >= 0 && i < w && j < h) {
						r.x = rect.x + (i - cameraX) * pw;
						r.y = rect.y + (j - cameraY) * ph;
						pixels[i][j].draw(r);
					}
				}
			}
		}

		public void draw() {
			clean();
			buildPixels();
			updateComposant();
			drawWorld();
			avatar.displayItems();
			printHeadMessage();
		}

		public void updateComposant() {
			for (Object o : lo) {
				o.updateComposant();
			}
		}

		public void buildPixels() {
			for (Object o : lo) {
				o.addComposantPixels();
			}
		}

		public void addPixel(Pixel p) {
			int x = p.from.getX(p.x);
			int y = p.from.getY(p.y);
			if (x < 0 || x >= w)
				return;
			if (y < 0 || y >= h)
				return;
			pixels[x][y].add(p);
		}

		public Pixel.Block isBlock(int x, int y) {
			if (y < 0)
				return Pixel.Block.EMPTY;
			if (x < 0)
			{
				return Pixel.Block.FULL;
			}
			if (x >= w || y >= h)
			{
				return Pixel.Block.FULL;
			}
			return pixels[x][y].isBlock();
		}

		public void drawBackground() {
			int r = Color.BACKGROUND.R;
			int g = Color.BACKGROUND.G;
			int b = Color.BACKGROUND.B;
			Rect tmp = new Rect(rect);
			tmp.h /= 10;
			for (int i = 0; i < 10; i++) {
				int rand = Math.abs(((Game.app.frame/7)+i)%3-1);
				r += 5-rand;
				g -= 5+rand;
				app.drawRect(tmp, r, g, b);
				tmp.y +=  tmp.h;
			}
		}

		public boolean isIn(Pixel.Type type, int x, int y) {
			if (x < 0 || y < 0 || x >= w || y >= h)
				return false;
			return pixels[x][y].isIn(type);
		}
}
