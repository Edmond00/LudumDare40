package gmtk;

import processing.core.*;

public class Sugar extends Takeable {

	Sugar(DesertLac desert, int x, int y) {
		super("sugar cane", desert, x, y);
		for (int i = 0; i < Game.app.rand(3,8); i++)
			new Pixel(Pixel.Type.SUGAR, this, 0, -i);
	}
}

