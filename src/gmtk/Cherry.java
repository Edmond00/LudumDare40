package gmtk;

import processing.core.*;

public class Cherry extends Takeable {

	Cherry(Foliage foliage, int x, int y) {
		super("cherry", foliage, x, y);
		new Pixel(Pixel.Type.CHERRY, this, 0, 0);
	}
}

