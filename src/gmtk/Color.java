package gmtk;

public enum Color {
		 BLACK(0, 0, 0),
		 WHITE(255, 255, 255, 255, 20),
		 RED(255, 15, 15, 255, 20),
		 BLUE(15, 15, 240, 255, 20),
		 PINK(240, 15, 240, 255, 20),
		 PURPLE(180, 100, 255, 255, 20),
		 YELLOW(255, 255, 50, 255, 20),
		 TEXT(255, 255, 255),
		 COMMENT(100, 100, 100),
		 INVISIBLE(0, 0, 0, 0),
		 WATER(50, 100, 230, 130, 10, 30),
		 SAND(220, 215, 130, 255, 30),
		 APPLE(200, 30, 10, 255, 30),
		 CHERRY(150, 0, 0, 255, 30),
		 CONE(150, 70, 20, 255, 30),
		 OLIVE(0, 0, 0, 255, 30),
		 EARTH(120, 60, 40, 255, 20),
		 DEEPEARTH(100, 30, 35, 255, 15),
		 WOOD(70, 30, 15, 255, 20),
		 CHERRYWOOD(40, 15, 10, 255, 20),
		 CONEWOOD(40, 25, 15, 255, 20),
		 OLIVEWOOD(30, 30, 30, 255, 20),
		 HERB(40, 120, 15, 255, 20),
		 SNOW(230, 230, 230, 255, 10),
		 STONE(60, 60, 60, 255, 20),
		 WHEAT(160, 170, 25, 80, 40, 80),
		 SUGAR(170, 60, 10, 80, 40, 80),
		 BAMBOO(200, 230, 50, 80, 30, 80),
		 LEAF(20, 150, 20, 200, 20, 50),
		 CHERRYLEAF(245, 180, 240, 200, 20, 50),
		 CONELEAF(10, 70, 10, 200, 20, 50),
		 OLIVELEAF(60, 60, 40, 200, 20, 50),
		 SKIN(230, 210, 150),
		 BACKGROUND(0, 100, 100);

		public int R;
		public int G;
		public int B;
		public int alpha;
		public int delta;
		public int adelta;

		//Constructeur
		Color(int r, int g, int b){
				this.R = r;
				this.G = g;
				this.B = b;
				this.alpha = 255;
				this.delta = 0;
				this.adelta = 0;
		}
		Color(int r, int g, int b, int a){
				this.R = r;
				this.G = g;
				this.B = b;
				this.alpha = a;
				this.delta = 0;
				this.adelta = 0;
		}
		Color(int r, int g, int b, int a, int delta){
				this.R = r;
				this.G = g;
				this.B = b;
				this.alpha = a;
				this.delta = delta;
				this.adelta = 0;
		}
		Color(int r, int g, int b, int a, int delta, int adelta){
				this.R = r;
				this.G = g;
				this.B = b;
				this.alpha = a;
				this.delta = delta;
				this.adelta = adelta;
		}

		public int getR() {
			int tmp = R + Game.app.rand(-delta, delta);
			if (tmp < 0) tmp = 0;
			if (tmp > 255) tmp = 255;
			return tmp;
		}

		public int getG() {
			int tmp = G + Game.app.rand(-delta, delta);
			if (tmp < 0) tmp = 0;
			if (tmp > 255) tmp = 255;
			return tmp;
		}

		public int getB() {
			int tmp = B + Game.app.rand(-delta, delta);
			if (tmp < 0) tmp = 0;
			if (tmp > 255) tmp = 255;
			return tmp;
		}

		public int getAlpha() {
			int tmp = alpha + Game.app.rand(-adelta, adelta);
			if (tmp < 0) tmp = 0;
			if (tmp > 255) tmp = 255;
			return tmp;
		}

		int rand(int r, int delta) {
			if (delta <= 0)
				return 0;
			return r % delta*2 - delta + Game.app.rand(5) - 2;
		}

		public int getR(int r) {
			int tmp = R + rand(r, delta);
			if (tmp < 0) tmp = 0;
			if (tmp > 255) tmp = 255;
			return tmp;
		}

		public int getG(int r) {
			int tmp = G + rand(r, delta);
			if (tmp < 0) tmp = 0;
			if (tmp > 255) tmp = 255;
			return tmp;
		}

		public int getB(int r) {
			int tmp = B + rand(r, delta);
			if (tmp < 0) tmp = 0;
			if (tmp > 255) tmp = 255;
			return tmp;
		}

		public int getAlpha(int r) {
			int tmp = alpha + rand(r, adelta);
			if (tmp < 0) tmp = 0;
			if (tmp > 255) tmp = 255;
			return tmp;
		}
}
