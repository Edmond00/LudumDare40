package gmtk;

import processing.core.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

public class Avatar extends Object{

		public enum State {
			STOP(),
			MOVE(),
			JUMP(),
			FALL();
		}

		public enum Move {
			LEFT(),
			RIGHT(),
			NO();
		}

		public State state;
		public Move move;
		public Takeable canBeTake;
		int jumping;
		int jumpHeigth;
		int speed;
		boolean wallLeft, wallRight;
		boolean inWater;
		Body body;
		Map<String, Item> items;

		void addItem(String name) {
			Item i = items.get(name);
			if (i != null)
				i.count += 1;
			else
				items.put(name, new Item(false, 1, 0));
		}

		void addGoal(String name, int goal) {
			Item i = items.get(name);
			if (i != null) {
				i.goal = goal;
				i.print = true;
			}
			else
				items.put(name, new Item(true, 0, goal));
		}

		boolean hasToGoal(String name) {
			Item i = items.get(name);
			if (i == null)
				return false;
			if (i.goal > 0)
				return true;
			return false;
		}

		void printItem(String name, Item i, Color c, int size, int x, int y) {
			Game.app.printText(name+"("+String.valueOf(i.count)+"/"+String.valueOf(i.goal)+")", size, x, y, c);
		}

		void displayItems() {
			int x, y;
			y = Game.app.sh/5*4;
			int gap = 20;
			int size = 15;
			int i = 0;
			Item tmp;
			for (Map.Entry<String, Item> entry : items.entrySet()) {
				tmp = entry.getValue();
				if (tmp.print) {
					x = Game.app.sw/4 * (i%3+1);
					if (tmp.count >= tmp.goal)
						printItem(entry.getKey(), tmp, Color.COMMENT, size, x, y);
					else
						printItem(entry.getKey(), tmp, Color.TEXT, size, x, y);
					i++;
					if (i%3 ==0)
						y += gap;
				}
			}
		}
		
		public abstract class Member extends Object {
			Avatar avatar;

			Member(Avatar a, int x, int y) {
				super(a, x, y);
				this.avatar = a;
			}

			void stop() {}
			void move() {}
			void jump() {}
			void fall() {}


			public void update() {
				if (Game.app.frame % 2 != 0)
					return ;
				switch(avatar.state) {
					case STOP:
						stop();
						break;
					case MOVE:
						move();
						break;
					case JUMP:
						jump();
						break;
					case FALL:
						fall();
						break;
				}
			}
		}

		public class Hat extends Member {
			Hat(Avatar a) {
				super(a, 0, -4);
				new Pixel(Pixel.Type.HAT, this, 0, 0);
			}
			public void touchCeilling(boolean touch) {
				avatar.touchCeilling(touch);
			}
			void fall() {
				pixels.get(0).y = -1;
			}
			void stop() {
				pixels.get(0).y = 0;
			}
			void move() {
				pixels.get(0).y = 0;
			}
			void jump() {
				pixels.get(0).y = 0;
			}
		}

		public class Head extends Member {
			Head(Avatar a) {
				super(a, 0, -3);
				new Pixel(Pixel.Type.HEAD, this, 0, 0);
			}
			public void touchLeftWall(boolean touch) {
				avatar.touchLeftWall(touch);
			}
			public void touchRightWall(boolean touch) {
				avatar.touchRightWall(touch);
			}
		}

		public class Hand extends Member {
			Hand(Avatar a) {
				super(a, 0, -1);
				new Pixel(Pixel.Type.HAND, this, -1, 0);
				new Pixel(Pixel.Type.HAND, this, 1, 0);
			}

			void spread() {
				pixels.get(0).x = -1;
				pixels.get(0).y = 0;
				pixels.get(1).x = 1;
				pixels.get(1).y = 0;
			}

			void align() {
				pixels.get(0).x = 0;
				pixels.get(0).y = 0;
				pixels.get(1).x = 0;
				pixels.get(1).y = 0;
			}

			void move() {
				if (pixels.get(0).x == 0)
					spread();
				else
					align();
			}
			void jump() {
				align();
			}
			void fall() {
				spread();
			}
			void stop() {
				spread();
			}
		}

		public class Body extends Member {
			Body(Avatar a) {
				super(a, 0, -2);
				new Pixel(Pixel.Type.BODY, this, 0, 0);
				new Pixel(Pixel.Type.BODY, this, 0, 1);
				new Pixel(Pixel.Type.HIP, this, 0, 2);
			}
			public void touchLeftWall(boolean touch) {
				avatar.touchLeftWall(touch);
			}
			public void touchRightWall(boolean touch) {
				avatar.touchRightWall(touch);
			}
			public void touchGround(boolean touch) {
				avatar.touchGround(touch);
			}
		}

		public class Shoe extends Member {
			Shoe(Avatar a) {
				super(a, 0, 0);
				new Pixel(Pixel.Type.SHOE, this, 0, 0);
				new Pixel(Pixel.Type.SHOE, this, 0, 0);
			}

			void spread() {
				pixels.get(0).x = -1;
				pixels.get(0).y = 0;
				pixels.get(1).x = 1;
				pixels.get(1).y = 0;
			}

			void align() {
				pixels.get(0).x = 0;
				pixels.get(0).y = 0;
				pixels.get(1).x = 0;
				pixels.get(1).y = 0;
			}

			void move() {
				if (pixels.get(0).x == 0)
					spread();
				else
					align();
			}
			void jump() {
				spread();
			}
			void fall() {
				align();
			}
			void stop() {
				align();
			}
		}

		public Avatar(MainScreen s, int x, int y){
			super(s, x, y);
			items = new HashMap();
			state = State.FALL;
			move = Move.NO;
			canBeTake = null;
			speed = 1;
			jumpHeigth = 10;
			jumping = -1;
			wallLeft = false;
			wallRight = false;
			inWater = false;
			addGoal("white flower", 1);
			new Shoe(this);
			body = new Body(this);
			new Hat(this);
			new Hand(this);
			new Head(this);
		}

		public void touchCeilling(boolean touch) {
			if (touch && jumping > 0) {
				jumping = 0;
			}
		}
		public void touchGround(boolean touch) {
			if (touch && jumping < 0) {
				jumping = 0;
			} else if (!touch && jumping == 0) {
				jumping = -1;
			}
		}
		public void touchLeftWall(boolean touch) {
			if (touch && move != Move.RIGHT) {
//				move = Move.NO;
				wallLeft = true;
			}
		}
		public void touchRightWall(boolean touch) {
			if (touch && move != Move.LEFT) {
//				move = Move.NO;
				wallRight = true;
			}
		}

		public void clean() {
			canBeTake = null;
			wallRight = false;
			wallLeft = false;
		}

		public void updateCamera() {
			if (screen.cameraX > 0 && x - screen.cameraX < screen.getW() / 3)
				screen.cameraX = x - screen.getW() / 3;
			if (screen.cameraY > 0 && y - screen.cameraY < screen.getH() / 3)
				screen.cameraY = y - screen.getH() / 3;
			if (screen.cameraX < screen.w - screen.getW()  && x - screen.cameraX > screen.getW() / 3 * 2)
				screen.cameraX = x - screen.getW() / 3 * 2;

			if (screen.cameraY < screen.h - screen.getH()  && y - screen.cameraY > screen.getH() / 3 * 2) {
				screen.cameraY = y - screen.getH() / 3 * 2;
			}
			if (screen.cameraY > screen.h - screen.getH())
				screen.cameraY = screen.h - screen.getH();
			if (screen.cameraX > screen.w - screen.getW())
				screen.cameraX = screen.w - screen.getW();
			if (screen.cameraX < 0)
				screen.cameraX = 0;
			if (screen.cameraY < 0)
				screen.cameraY = 0;
		}

		public void splash() {
			new Particule(Pixel.Type.SPLASH, body.pixels.get(2), false, -2, 3, -1, -3, 6);
		}

		public void flush() {
			new Particule(Pixel.Type.FLUSH, body.pixels.get(2), false, -2, 3, 0, -2, 2);
		}
		public void flushSnow() {
			new Particule(Pixel.Type.FLUSHSNOW, body.pixels.get(2), false, -2, 3, 0, -2, 2);
		}

		public void update() {
			boolean oldInWater = inWater;
			inWater = screen.isIn(Pixel.Type.WATER, x, y);
			if (inWater != oldInWater && inWater) {
				for (int i = 0; i < Game.app.rand(4, 7); i++) {
					splash();
				}
			}
			if (screen.isIn(Pixel.Type.SAND, x, y+1) && state == State.MOVE) {
				flush();
			}
			if (screen.isIn(Pixel.Type.SNOW, x, y+1) && state == State.MOVE) {
				flushSnow();
			}
			if (goalComplete())
				Game.app.changeScreen("narration");
			updateCamera();
			if (inWater && Game.app.frame % 4 != 0)
				return ;
			if (jumping < 0)
				y += speed;
			if (jumping > 0) {
				y -= speed;
				jumping += 1;
			}
			if (jumping > jumpHeigth) {
				jumping = -1;
			}
			if (move == Move.LEFT && !wallLeft)
				x -= speed;
			if (move == Move.RIGHT && !wallRight)
				x += speed;
			if (jumping > 0)
				state = State.JUMP;
			else if (jumping < 0)
				state = State.FALL;
			else if (move == Move.NO)
				state = State.STOP;
			else
				state = State.MOVE;
		}

		boolean goalComplete() {
		for (Map.Entry<String, Item> entry : items.entrySet()) {
			if (entry.getValue().count < entry.getValue().goal)
				return false;
		}
		return true;
		}
}
