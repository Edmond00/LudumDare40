package gmtk;

import processing.core.*;
import java.util.ArrayList;
import java.util.List;

abstract class Ground extends Object{

		static int lastHeigth = 75;
		static int south = 0;
		static int width = 50;
		static int heigth = 100;
		static int desertWidth = 30;
		static int montainWidth = 30;
		static int transition = width*3;
		static int transitionDesert = transition;

		int id;

		public Ground(MainScreen s, int n) {
			super(s, n*width, 0);
			id = n;
		}

		public void update() {
		}

		public boolean updateMe() {
			if (Math.abs(screen.cameraX - x) <  screen.getW() + width)
				return true;
			return false;
		}

		public Pixel.Type getEarthType(int level) {
			int north = (screen.numberGround-montainWidth)*width;
			int altitude = level + lastHeigth;
			int inNorth = south - north + Game.app.rand(transition) + 700;
			if (level == 0) {
				south += 1;
			}
			if (south < desertWidth*width)
				return Pixel.Type.SAND;
			if (transitionDesert > 0) {
				if (level == 0)
					transitionDesert -= 1;
//				Game.app.log(String.valueOf(transitionDesert));
				if (Game.app.rand(transition) < transitionDesert)
				return Pixel.Type.SAND;
			}
			if (level == 0) {
				if (inNorth > 0)
					return Pixel.Type.SNOW;
				return Pixel.Type.HERB;
			} else if (level < 5) {
				if (inNorth > 0)
					return Pixel.Type.STONE;
				return Pixel.Type.EARTH;
			} else {
				if (inNorth > 0)
					return Pixel.Type.STONE;
				return Pixel.Type.DEEPEARTH;
			}
		}
}

