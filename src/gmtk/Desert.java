package gmtk;

import processing.core.*;
import java.util.ArrayList;
import java.util.List;

class Desert extends Ground {
		

		public Desert(MainScreen s, int n) {
			super(s, n);
			int flat = 0;
			Tree.Type type;
			type = Tree.Type.OLIVE;
			for (int i = 0; i < width; i++) {
				if ((int)Game.app.random(7) == 0) {
					flat = 0;
					lastHeigth += ((int)Game.app.random(-3, 4) / 2);
					if (lastHeigth < 30)
						lastHeigth = 30;
					if (lastHeigth > 95)
						lastHeigth = 95;
				} else {
					flat++;
				}
				if (flat > 6 && (int)Game.app.random(8) == 0) {
					flat = 0;
					new Tree(this, i-6, lastHeigth - 1, type);
				}
				for (int j = lastHeigth; j < heigth; j++) {
					new Pixel(getEarthType(j - lastHeigth), this, i, j);
				}
			}
		}

}

