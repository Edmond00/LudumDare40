package gmtk;

import processing.core.*;

public class Olive extends Takeable {

	Olive(Foliage foliage, int x, int y) {
		super("olive", foliage, x, y);
		new Pixel(Pixel.Type.OLIVE, this, 0, 0);
	}
}

