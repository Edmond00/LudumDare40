package gmtk;

public class Rect {

		Integer x, y, w, h;

		public Rect(int ax, int ay, int aw, int ah){
			x = ax;
			y = ay;
			w = aw;
			h = ah;
		}
		public Rect(Rect r) {
			x = r.x;
			y = r.y;
			w = r.w;
			h = r.h;
		}
		public Rect() {
			x = 0;
			y = 0;
			w = 0;
			h = 0;
		}
}
