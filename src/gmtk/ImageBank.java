package gmtk;

import java.util.Hashtable;
import processing.core.*;

public class ImageBank {
	
	Hashtable<String, PImage> bank;

	ImageBank() {
		bank = new Hashtable<String, PImage>();
//		add("pig");
	}

	void add(String file) {
		PImage tmp = Game.app.loadImage("./data/img/" + file + ".png");
		bank.put(file, tmp);
	}

	void addLink(String link, String key) {
		PImage img = bank.get(key);
		if (img == null)
			return ;
		bank.put(link, img);
	}

	PImage get(String key) {
		return bank.get(key);
	}
}

