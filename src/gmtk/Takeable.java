package gmtk;

import processing.core.*;

public class Takeable extends Object {
		String name;

		public Takeable(String name, Object parent, int x, int y){
			super(parent, x, y);
			this.name = name;
		}
		
		boolean canBeTake(Avatar avatar) {
			if (Math.abs(avatar.x - getX(0)) > 3)
				return false;
			if (Math.abs(avatar.y - getY(0)) > 3)
				return false;
			return true;
		}
		public String takeMessage() {
			return new String("Press SPACE to take " + name);
		}
		public void update() {
			if (canBeTake(screen.avatar) && screen.avatar.hasToGoal(name)) {
				screen.actionMessage = takeMessage(); 
				screen.avatar.canBeTake = this;
			}
		}
}

