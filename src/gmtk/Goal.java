package gmtk;

import processing.core.*;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Map;
import java.util.HashMap;

public class Goal {
	Queue<String> messages;
	Map<String, Integer> goals;
	String hint;
	int zoom;

	public Goal(String hint, int zoom){
		messages = new LinkedList<String>();
		goals = new HashMap<String, Integer>();
		this.hint = hint;
		this.zoom = zoom;
	}

	public void pushMessage(String str) {
		messages.add(str);
	}

	public void pushGoal(String str,int n) {
		goals.put(str, n);
	}

}
