package gmtk;

import processing.core.*;
import java.util.ArrayList;
import java.util.List;

class Mountain extends Ground {
		
		void addRandomFlower(int x, int y) {
			int r = Game.app.rand(10);
			if (r < 8)
				new Flower(this, x, y, Flower.Type.BLUE);
			else
				new Flower(this, x, y, Flower.Type.PURPLE);
		}
		

		public Mountain(MainScreen s, int n) {
			super(s, n);
			int flat = 0;
			int lastFlower = 0;
			Tree.Type type;
			type = Tree.Type.CONE;
			for (int i = 0; i < width; i++) {
				if ((int)Game.app.random(3) == 0) {
					flat = 0;
					if (lastHeigth > 60)
						lastHeigth += Math.abs((int)Game.app.random(0, 6)) * -1;
					else
						lastHeigth += ((int)Game.app.random(-8, 8));
					if (lastHeigth < 30) {
						lastHeigth = 30;
						lastHeigth += ((int)Game.app.random(0, 8));
					}
					if (lastHeigth > 95) {
						lastHeigth = 95;
					}
				} else {
					flat++;
				}
				if (flat >= 6) {
					flat = 0;
					new Tree(this, i-6, lastHeigth - 1, type);
				} else if (lastFlower > 3 && Game.app.rand(16) == 0) {
					lastFlower = 0;
					addRandomFlower(i, lastHeigth - 1);
				}
				lastFlower +=1;
				for (int j = lastHeigth; j < heigth; j++) {
					new Pixel(getEarthType(j - lastHeigth), this, i, j);
				}
			}
		}

}

