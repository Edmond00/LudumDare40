package gmtk;

import processing.core.*;
import java.util.ArrayList;
import java.util.List;

abstract class Object {

		MainScreen screen;
		Object parent;
		boolean toRemove;
		List<Object> lo;
		List<Pixel> pixels;
		int x, y;

		public void initialize(int x, int y) {
			lo = new ArrayList<Object>();
			pixels = new ArrayList<Pixel>();
			this.x = x;
			this.y = y;
		}

		public Object(MainScreen s, int x, int y){
			toRemove = false;
			parent = null;
			screen = s;
			screen.lo.add(this);
			initialize(x, y);
		}

		public Object(Object parent, int x, int y){
			toRemove = false;
			this.parent = parent;
			screen = parent.screen;
			parent.lo.add(this);
			initialize(x, y);
		}

		public void addComposantPixels(){
			if (updateMe() ==false)
				return ;
			for(Pixel p : pixels) {
				screen.addPixel(p);
			}
			for(Object obj : lo) {
				obj.addComposantPixels();
			}
		}

		public void updateComposant(){
			if (updateMe() ==false)
				return ;
			for(int i = 0; i < pixels.size(); i++) {
				pixels.get(i).update();
			}
			for(Object obj : lo) {
				obj.updateComposant();
			}
			update();
		}

		public void cleanComposant(){
			if (updateMe() ==false)
				return ;
			clean();
			for(Object obj : lo) {
				obj.cleanComposant();
			}
			lo.removeIf(o -> o.toRemove);
			pixels.removeIf(o -> o.toRemove);
		}

		int getX(int x) {
			x += this.x;
			if (parent != null)
				return parent.getX(x);
			return x;
		}

		int getY(int y) {
			y += this.y;
			if (parent != null)
				return parent.getY(y);
			return y;
		}

		public void remove() {
			toRemove = true;
	//		for(Object obj : lo) {
	//			obj.remove();
	//		}
	//		if (parent != null)
	//			parent.lo.remove(this);
	//		pixels.clear();
	//		lo.clear();
		}

		public Color getColor(){return Color.INVISIBLE;}
		public boolean updateMe(){return true;}
		public void update(){}
		public void clean(){}
		public void touchGround(boolean touch){}
		public void touchCeilling(boolean touch){}
		public void touchLeftWall(boolean touch){}
		public void touchRightWall(boolean touch){}
}
