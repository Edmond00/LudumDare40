package gmtk;

import processing.core.*;

public class Wheat extends Takeable {

	Wheat(Field field, int x, int y) {
		super("wheat", field, x, y);
		for (int i = 0; i < Game.app.rand(2,6); i++)
			new Pixel(Pixel.Type.WHEAT, this, 0, -i);
	}
}

