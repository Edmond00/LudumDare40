package gmtk;

import processing.core.*;

public class Flower extends Takeable {
	public enum Type {
		BLUE("blue flower", Color.BLUE),
		RED("red flower", Color.RED),
		PINK("pink flower", Color.PINK),
		PURPLE("purple flower", Color.PURPLE),
		WHITE("white flower", Color.WHITE);
		String name;
		Color color;

		Type(String name, Color c) {
			this.name = name;
			this.color = c;
		}
	}

	public Type type;

	Flower(Ground ground, int x, int y, Type type) {
		super(type.name, ground, x, y);
		this.type = type;
		new Pixel(Pixel.Type.STALK, this, 0, 0);
		new Pixel(Pixel.Type.FLOWER, this, Game.app.rand(3) - 1, -1);
	}
	public Color getColor(){return type.color;}
}
