package gmtk;

import processing.core.*;

public class Apple extends Takeable {

	Apple(Foliage foliage, int x, int y) {
		super("apple", foliage, x, y);
		new Pixel(Pixel.Type.APPLE, this, 0, 0);
	}
}

