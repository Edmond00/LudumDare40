package gmtk;

import processing.core.*;
import java.util.ArrayList;
import java.util.List;

class Tree extends Object {

		public enum Type {
			CONE(Pixel.Type.CONEWOOD, Pixel.Type.CONELEAF, Pixel.Type.CONELEAF),
			CHERRY(Pixel.Type.CHERRYWOOD, Pixel.Type.CHERRYLEAF, Pixel.Type.CHERRYDEADLEAF),
			APPLE(Pixel.Type.WOOD, Pixel.Type.LEAF, Pixel.Type.DEADLEAF),
			OLIVE(Pixel.Type.OLIVEWOOD, Pixel.Type.OLIVELEAF, Pixel.Type.OLIVEDEADLEAF);

			Pixel.Type tree;
			Pixel.Type leaf;
			Pixel.Type deadleaf;

			Type(Pixel.Type tree, Pixel.Type leaf, Pixel.Type deadleaf) {
				this.tree = tree;
				this.leaf = leaf;
				this.deadleaf = deadleaf;
			}
		}

		public Type type;

		public Tree(Ground ground, int x, int y, Type type){
			super(ground, x, y);
			this.type = type;
			generate();
		}

		public void generate() {
			int heigth, width;
			width = Game.app.rand(3, 6);
			new Pixel(type.tree, this, -1, 0);
			new Pixel(type.tree, this, width, 0);
			for (int i = 0; i < width; i++) {
				int tmpX = i;
				heigth = width*2 + Game.app.rand(10, 25);
			 	for (int j = 0; j < heigth; j++) {
					new Pixel(type.tree, this, tmpX, -j);
					if (j > width*2)
						tmpX += Game.app.rand(-2,2);
					else if (j > 2 && Game.app.rand(2) == 0)
						tmpX += Game.app.rand(-2,2);
					else if (j > 2 && Game.app.rand(3) == 0)
						tmpX += Game.app.rand(-2,2);
				}
			}
			new Foliage(this, width/2, width*-2-15, width);

		}

		public void update(){}
}

