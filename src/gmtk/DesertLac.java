package gmtk;

import processing.core.*;
import java.util.ArrayList;
import java.util.List;

class DesertLac extends Ground {

		public DesertLac(MainScreen s, int n) {
			super(s, n);
			int begin = Game.app.rand(10, width/3);
			int end = Game.app.rand((width/3)*2, width-10);
			int lastBegin = begin;
			int lastEnd = end;
			int r;
			for (int j = lastHeigth; j < heigth; j++) {
				r = Game.app.rand(8);
				for (int i = 0; i < width; i++) {
					if (i > begin && i < end)
						new Pixel(Pixel.Type.WATER, this, i, j);
					else if (i > begin - 3 && i < end + 3)
						new Pixel(Pixel.Type.SAND, this, i, j);
					else
						new Pixel(getEarthType(j - lastHeigth), this, i, j);
					if ((i <= begin || i >= end) && j == lastHeigth && Game.app.rand(3) == 0) {
						new Sugar(this, i, j - 1);
					}
				}
				lastBegin = begin;
				lastEnd = end;
				begin += Game.app.rand(4);
				end -= Game.app.rand(4);

			}
		}

}

