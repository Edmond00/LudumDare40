package gmtk;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.LinkedList;
import processing.core.*;
import ddf.minim.*;

public class Game extends PApplet{

		Minim minim;
		ImageBank bank;
		SoundBank sounds;
		static Game app = null;
		List<Screen> ls;
		Queue<Goal> goals;
		Screen actualScreen;
		Rect screen;
		MainScreen main;
		NarrationScreen narration;
		int sw;
		int sh;
		int time;
		int frame;

		PFont font;


		void chargeGoals() {
			Goal tmp;

			tmp = new Goal("find some red flowers", 10);
			tmp.pushMessage("The white flower was pretty...");
			tmp.pushMessage("...but the red flowers look more intersting.");
			tmp.pushGoal("red flower", 5);
			tmp.pushGoal("white flower", 1);
			goals.add(tmp);

			tmp = new Goal("find others plants", 8);
			tmp.pushMessage("The man quickly got bored with flowers.");
			tmp.pushMessage("He looked for other plants to study.");
			tmp.pushGoal("apple", 5);
			tmp.pushGoal("wheat", 5);
			goals.add(tmp);

			tmp = new Goal("Explore your environment", 6);
			tmp.pushMessage("He was always looking for something new.");
			tmp.pushGoal("bamboo", 5);
			tmp.pushGoal("cherry", 5);
			tmp.pushGoal("pink flower", 3);
			goals.add(tmp);

			tmp = new Goal("go to the desert on your left", 4);
			tmp.pushMessage("The more he explored this world...");
			tmp.pushMessage("The more the world seemed big.");
			tmp.pushGoal("sugar cane", 5);
			tmp.pushGoal("olive", 5);
			goals.add(tmp);

			tmp = new Goal("go to the mountain on your right", 2);
			tmp.pushMessage("The more he learned about this world...");
			tmp.pushMessage("The more the world seemed complex.");
			tmp.pushGoal("blue flower", 5);
			tmp.pushGoal("purple flower", 5);
			tmp.pushGoal("pine cone", 5);
			goals.add(tmp);

			tmp = new Goal("", 2);
			tmp.pushMessage("The man remenbered...");
			tmp.pushMessage("...when he was interested by a simple flower....");
			tmp.pushMessage("...but this moment seemed far away.");
			tmp.pushMessage("THE END");
			goals.add(tmp);
		}



		public void drawLine(int x, int y, int x2, int y2, int w, Color c) {
			strokeWeight(w);
			fill(c.R, c.G, c.B);
			stroke(c.R, c.G, c.B);
			line(x, y, x2, y2);
			strokeWeight(1);
		}

		public void drawRect(Rect r, Color c) {
			strokeWeight(0);
//			stroke(c.getR(), c.getG(), c.getB(), c.getAlpha());
			fill(c.getR(), c.getG(), c.getB(), c.getAlpha());
			rect(r.x, r.y, r.w, r.h);
		}
		public void drawRect(Rect r, Color c, Pixel p) {
			strokeWeight(0);
//			stroke(c.getR(), c.getG(), c.getB(), c.getAlpha());
			fill(c.getR(p.rr), c.getG(p.rg), c.getB(p.rb), c.getAlpha(p.ra));
			rect(r.x, r.y, r.w, r.h);
		}
		public void drawRectAlpha(Rect r, Color c, Pixel p, int alpha) {
			strokeWeight(0);
			fill(c.getR(p.rr), c.getG(p.rg), c.getB(p.rb), alpha);
			rect(r.x, r.y, r.w, r.h);
		}
		public void drawRect(Rect rect, int r, int g, int b) {
			fill(r,g,b, 255);
			stroke(r,g,b, 255);
			rect(rect.x, rect.y, rect.w, rect.h);
		}

		public void drawRect(int x, int y, int L, Color c) {
			fill(c.R, c.G, c.B, c.alpha);
			stroke(c.R, c.G, c.B, c.alpha);
			rect(x-L/2, y-L/2, L, L);
		}

		public void background(int r, int g, int b) {
			fill(r, g, b);
			stroke(r, g, b);
			rect(0, 0, screen.w, screen.h);
		}

		public void background(Color c) {
			fill(c.R, c.G, c.B, c.alpha);
			stroke(c.R, c.G, c.B, c.alpha);
			rect(0, 0, screen.w, screen.h);
		}

		public void log(String str) {
			System.out.println(str);
		}

		public static void main(String[] args) {
				PApplet.main("gmtk.Game");
		}

		public void settings(){
				app = this;
				sw = 1000;
				sh = 700;
				screen = new Rect(0,0,sw,sh);
				size(screen.w, screen.h);
		}

		public int rand(int n) {
			return (int)random(n);
		}

		public int rand(int min, int max) {
			return (int)random(min, max);
		}

		public void printText(String str, int size, int x, int y, int r, int g, int b)
		{
			fill(r, g, b);
			textSize(size);
			text(str, x, y);
		}
		public void printText(String str, int size, int x, int y, Color c)
		{
			printText(str, size, x, y, c.R, c.G, c.B);
		}

		public void setup(){
			goals = new LinkedList<Goal>();
			chargeGoals();
			frame = 0;
			time = millis();
			minim = new Minim(this);
			bank = new ImageBank();
			sounds = new SoundBank();
			sounds.loop("narration");
			sounds.loop("exploration");
			sounds.mute("narration");
			sounds.unmute("exploration");
			ls = new ArrayList<Screen>();
			main = new MainScreen(this, "main", 18, 18);
			narration = new NarrationScreen(this, "narration");
//			changeScreen("narration");
			font = loadFont("./data/font/Copperplate-Bold-70.vlw");
			textFont(font);
			textAlign(CENTER, CENTER);
		}

		public void event() {
			if (keyPressed && key != CODED)
				actualScreen.asciiEvent(key);
			if (keyPressed && key == CODED)
				actualScreen.codedEvent(keyCode);
		}

//		public boolean canBePressed() {
//			if (keyPressed && key == CODED &&
//				(keyCode == app.LEFT || keyCode == app.RIGHT))
//				return true;
//			return false;
//		}

		public void draw(){
				if (millis() > time + 25) {
					background(0,0,0);
					time = millis();
//					Game.app.log("start");
					actualScreen.draw();
					frame += 1;
				}
//				if (canBePressed())
//					event();
				if (keyPressed == false)
				{
					key = 0;
					keyCode = 0;
				}
		}

		public void keyPressed() {
//			if (!canBePressed())
				event();
		}

		public void keyReleased() {
			if (key == CODED)
				actualScreen.codedEventReleased(keyCode);
			else
				actualScreen.asciiEventReleased(key);
		}

		public void changeScreen(String name)
		{
			if (actualScreen == narration) {
//				sounds.mute("narration");
//				sounds.unmute("exploration");
				if (goals.peek() == null)
					exit();
			} else {
//				sounds.mute("exploration");
//				sounds.unmute("narration");
				Goal g = goals.poll();
				main.hintMessage = g.hint;
				main.pw = g.zoom;
				main.ph = g.zoom;
				main.cameraX = 0;
				main.cameraY = 0;
				main.resize();
				main.avatar.updateCamera();
				for (String str : g.messages) {
					narration.pushText(str);
				}
				for (Map.Entry<String, Integer> e : g.goals.entrySet()) {
					main.avatar.addGoal(e.getKey(), e.getValue());
				}
			}
			for (Screen screen : ls) {
				if (screen.name.equals(name))
					actualScreen = screen;
			}
		}

}
