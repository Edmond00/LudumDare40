package gmtk;

import processing.core.*;

public class Text {

		String str;
		Integer size;
		Integer r, g, b;
		int x, y;

		public Text(int x, int y, String astr, int asize, Color color) {
			this.x = x;
			this.y = y;
			str = astr;
			size = asize;
			r = color.R;
			g = color.G;
			b = color.B;
		}

		void changeColor(Color color)
		{
			r = color.R;
			g = color.G;
			b = color.B;
		}

		public void draw() {
			Game.app.printText(str, size, x, y, r, g, b);
		}
}

