package gmtk;

import processing.core.*;

public class Particule extends Pixel {
	int minVX, maxVX;
	int minVY, maxVY;
	int lifetime;

	public Particule(Type t, Pixel origin, boolean removeOld, int minX, int maxX, int minY, int maxY) {
		this(t, origin, removeOld, minX, maxX, minY, maxY, -1);
	}
	public Particule(Type t, Pixel origin, boolean removeOld, int minX, int maxX, int minY, int maxY, int lifetime) {
		super(t, origin, removeOld);
		this.minVX = minX;
		this.maxVX = maxX;
		this.minVY = minY;
		this.maxVY = maxY;
		this.lifetime = lifetime;
	}
		
	public void special() {
		if (lifetime == 0)
			remove();
		if (lifetime > 0)
			lifetime--;
		x += Game.app.rand(minVX, maxVX);
		y += Game.app.rand(minVY, maxVY);
		if (from.screen.isBlock(getX(), getY()) == Block.FULL)
			remove();
	}
}

