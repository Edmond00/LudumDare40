package gmtk;

import processing.core.*;
import java.util.ArrayList;
import java.util.List;

class Foliage extends Object {

		public Tree.Type type;

		public Foliage(Tree tree, int x, int y, int width){
			super(tree, x, y);
			type = tree.type;
			generate(width);
		}

		public void generate(int treeWidth) {
			int heigth, width;
			heigth = Game.app.rand(15, 25);
			int down = 0;
			int tmpW;
			for (int j = heigth/-2; j < heigth/2; j++) {
				width = treeWidth + Game.app.rand(10, 20);
				if (down < 5)
					tmpW = 4;
				if (down < 10)
					tmpW = 3;
				else
					tmpW = 2;

				for (int i = width/-tmpW; i < width/tmpW; i++) {
					if (Game.app.rand(3) > 0) {
						new Leaf(type.leaf, this, i, j, type.deadleaf);
						if (type == Tree.Type.APPLE && Game.app.rand(200) == 0) {
							new Apple(this, i, j);
						}
						if (type == Tree.Type.CHERRY && Game.app.rand(30) == 0) {
							new Cherry(this, i, j);
						}
						if (type == Tree.Type.OLIVE && Game.app.rand(80) == 0) {
							new Olive(this, i, j);
						}
						if (type == Tree.Type.CONE && Game.app.rand(140) == 0) {
							new Cone(this, i, j);
						}
					}
				}
				down++;
			}

		}

		public void update(){}
}
