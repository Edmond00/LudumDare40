package gmtk;

import processing.core.*;

public class Leaf extends Pixel {

	public Pixel.Type deadType;
	public Leaf(Type t, Object o, int x, int y, Pixel.Type deadType) {
		super(t, o, x, y);
		this.deadType = deadType;
	}
		
	public void special() {
		if (from.screen.isIn(Pixel.Type.HEAD, getX(), getY())) {
			new Particule(deadType, this, true, -2, 3, 0, 3);
		}
	}
}


